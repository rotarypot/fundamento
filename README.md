# README #

### Que contiene este repo ? ###

Este repo es un proyecto basico para armar html estatico usando un workflow de desarrollo con gulp, basado en Foundation for sites (SASS)

### Como hago el set up de esta madrola? ###

* Primero es necesario tener instalado [**npm**](https://www.npmjs.com/get-npm) globalmente.
* Tambien es necesario tener instalado globalmente, sigue las [**instrucciones para tu sistema operativo**](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)

Una ves instalado npm solo necesitas clonar el repo a tu computadora.

```
$ git clone https://.................016.git
```

Entra a ese directorio donde pusiste el repo

```
$ cd fundamento
```

Instala las dependencias

```
$ npm install
```

Corre gulp para lanzar el server local

```
$ gulp
```

A partir de este momento todos los cambios que realizes en el codigo del sitio son automaticamente procesados e inyectados al browser, en los casos que no son inyectados el browser es refrescado automagicamente.

#Tareas básicas#

##Builds##

###build sin procesar las imgs###
``$ gulp build-noimages
``

###build completo listo para subir###
``$ gulp build
``

###Tarea  default para desarrollo local###

Lanza los procesos necesarios para desarrollar localmente.

``$ gulp
``

###Proceso de imgs###
``$ gulp build-images
``