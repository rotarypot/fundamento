var gulp = require('gulp'),
    sass = require('gulp-sass'),
    useref = require('gulp-useref'),
    gulpIf = require('gulp-if'),
    cssnano = require('gulp-cssnano'),
    imagemin = require('gulp-imagemin'),
    cache = require('gulp-cache'),
    del = require('del'),
    autoprefixer = require('gulp-autoprefixer'),
    panini = require('panini'),
    uglify = require('gulp-uglify');

function onError(err) {
    console.log(err);
    this.emit('end');
}

gulp.task('html', function(done) {
    panini.refresh();
    gulp.src('app/pages/*.html')
        .pipe(panini({
            root: 'app/pages',
            layouts: 'app/layouts',
            partials: 'app/partials'
        }))
        .pipe(gulp.dest('./app'))
    done();
});

gulp.task('build-html', function(done) {
    panini.refresh();
    gulp.src('app/pages/*.html')
        .pipe(panini({
            root: 'app/pages',
            layouts: 'app/layouts',
            partials: 'app/partials'
        }))
        .pipe(gulp.dest('./build'));
    done();
});



var sassPaths = [
    'node_modules/foundation-sites/scss',
    'node_modules/motion-ui/src'
];

gulp.task('sass', function() {
    return gulp.src('app/scss/style.scss')
        .pipe(sass({
            includePaths: sassPaths
        }))
        .on('error', onError)
        .pipe(gulp.dest('app/css/'))
});

// CSS Auto prefixing.
gulp.task('styles', gulp.series('sass', function(done) {
    gulp.src('./app/css/style.css')
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./app/css'))
        .pipe(bs.stream());
    done();
}));

gulp.task('build-styles', gulp.series('sass', function(done) {
    gulp.src('./app/css/style.css')
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./build/css'))
    done();
}));

// minification de imgs
gulp.task('build-images', function(done) {
    gulp.src('./app/images/**/*.+(png|jpg|jpeg|gif|svg)')
        // Caching images that ran through imagemin
        .pipe(cache(imagemin()))
        .on('error', onError)
        .pipe(gulp.dest('./build/images'));
    done();
});

// concatenation de js y css ligados dentro del html
gulp.task('useref', function(done) {
    return gulp.src('./build/*.html')
        .pipe(useref({
            searchPath: ['/', '/build/']
        }))
        .pipe(gulpIf('*.css', cssnano()))
        .on('error', onError)
        .pipe(gulp.dest('./build'));
});


// inicializamos browserSync
var bs = require('browser-sync').create();
// inicializamos un server local usando BrowserSync
gulp.task('browserSync', function(done) {
    bs.init({
        notify: false,
        server: {
            baseDir: './app',
            routes: {
                '/node_modules': 'node_modules' /* key = la ruta que llamas en tu html, value = la ruta relativa a gulp.js*/
            } 
        },
        reloadDelay: 1000,
        files: ['./app/*.html', './app/js/*.js', './app/images/*']   
    });
    done();
})

gulp.task('uglyjs', function(done) {
    gulp.src('./build/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('./build/js/'))
    done()
})

// borramos files no usados
gulp.task('build-clean', function(done) {
    del.sync('./build');
    done();
})

// este task 'watch' ejecuta los tasks "prefixer y browserSync" ANTES DE LOS QUE ESTAN DENTRO DE LA FUNCION
gulp.task('watch', function(done) {
    gulp.watch('./app/scss/*.scss', gulp.series('styles'));
    gulp.watch('./app/pages/**/*', gulp.series('html'));
    gulp.watch(['./app/{layouts,partials}/**/*'], gulp.series('html'));
    done();
})

gulp.task('default', gulp.series('html', 'styles', 'browserSync', 'watch', function(done) {
    done();
}))

gulp.task('build', gulp.series('build-clean', gulp.parallel('build-html', 'build-styles', 'build-images'), 'useref', 'uglyjs',
    function(done) {
        done();
    }))

gulp.task('build-noimages', gulp.series('build-clean', gulp.parallel('build-html', 'build-styles'), 'useref', 'uglyjs',
    function(done) {
        done();
    }))

